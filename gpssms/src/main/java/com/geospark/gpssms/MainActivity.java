package com.geospark.gpssms;

import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private LocationListener _location_listener = null;
    private long _listen_interval = 10;
    private boolean _sending_location = false;
    private Handler _countdown_handler = null;
    private long _time_remaining = 0;
    private String _phone_number = "";

    private Runnable _countdown = new Runnable() {
        @Override
        public void run() {
            Resources res = getResources();
            String text = null;

            if (_time_remaining > 0) {
                text = String.format(res.getString(R.string.countdown), _time_remaining);
                _time_remaining--;
                // Add ourselves to the handler's message queue.
                _countdown_handler.postDelayed(this, 60 * 1000);
            } else {
                text = String.format(res.getString(R.string.pending));

                // Kick off the GPS.
                LocationManager location_manager = (LocationManager)MainActivity.this.getSystemService(MainActivity.LOCATION_SERVICE);
                location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, _location_listener);
            }

            TextView countdown = (TextView)findViewById(R.id.lbl_countdown);
            countdown.setText(text.toCharArray(), 0, text.length());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_start_stop = (Button)findViewById(R.id.btn_start_stop);
        btn_start_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sending_location = !_sending_location;

                Button btn = (Button)v;

                EditText phone_number = (EditText)findViewById(R.id.txt_phone_number);
                EditText interval = (EditText)findViewById(R.id.txt_interval);
                View countdown = findViewById(R.id.lbl_countdown);

                if (_sending_location) {
                    btn.setText(R.string.stop);
                    phone_number.setEnabled(false);
                    interval.setEnabled(false);
                    _listen_interval = Integer.parseInt(interval.getText().toString());
                    _phone_number = phone_number.getText().toString();
                    _time_remaining = 0;
                    // Fire off an update immediately.
                    _countdown_handler.post(_countdown);
                    countdown.setVisibility(View.VISIBLE);
                } else {
                    _countdown_handler.removeCallbacks(_countdown);
                    countdown.setVisibility(View.INVISIBLE);
                    btn.setText(R.string.start);
                    phone_number.setEnabled(true);
                    interval.setEnabled(true);
                }

            }
        });

        _location_listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                // Locations have "providers", these are normally "network" for cell/WiFi locations, and "gps" for GPS locations.
                // To save battery, we switch off the GPS until we need it. This does mean that it takes a few seconds to get
                // a new fix, and it probably won't take advantage of the previous fix.
                // Once we get a GPS location, switch off the GPS, send the SMS, and queue another update.
                // Could also do minimum accuracy checking here.
                // See: https://developer.android.com/guide/topics/location/strategies.html for lots of clever stuff.
                if (location.getProvider().equalsIgnoreCase("gps")) {
                    LocationManager location_manager = (LocationManager)MainActivity.this.getSystemService(MainActivity.LOCATION_SERVICE);
                    location_manager.removeUpdates(this);
                    send_message(location);

                    // If we press stop while we're sending a message, don't queue any more.
                    if (_sending_location) {
                        _time_remaining = _listen_interval;
//                        Resources res = getResources();
//                        String text = String.format(res.getString(R.string.countdown), _time_remaining);
//                        TextView countdown = (TextView)findViewById(R.id.lbl_countdown);
//                        countdown.setText(text.toCharArray(), 0, text.length());
                        _countdown_handler.post(_countdown);
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {}

            @Override
            public void onProviderDisabled(String provider) {}
        };

        // We use a handler to start the location gathering. When start is pressed, a Java runnable,
        // which enables GPS locations, is added to the handler's message queue.
        _countdown_handler = new Handler();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocationManager location_manager = (LocationManager)this.getSystemService(MainActivity.LOCATION_SERVICE);
        location_manager.removeUpdates(_location_listener);

        // Clear any pending updates.
        if (_countdown_handler != null && _countdown != null) {
            _countdown_handler.removeCallbacks(_countdown);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void send_message(Location location) {
        try {
            SmsManager mgr = SmsManager.getDefault();
            StringBuilder location_string = new StringBuilder();
            // Time is milliseconds since 1970 epoch. Not dreadfully accurate, but close enough.
            location_string.append(location.getTime());
            location_string.append(",");
            location_string.append(location.getLongitude());
            location_string.append(",");
            location_string.append(location.getLatitude());
            location_string.append(",");
            // Altitude in metres.
            location_string.append(location.getAltitude());
            location_string.append(",");
            // Accuracy is measured in metres as a radius from the computed position, with a 1-sigma confidence.
            location_string.append(location.getAccuracy());

            Log.d("LOCATION", location_string.toString());

            mgr.sendTextMessage(_phone_number, null, location_string.toString(), null, null);
        } catch (IllegalArgumentException e) {
            Toast.makeText(MainActivity.this, R.string.cannot_send, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
